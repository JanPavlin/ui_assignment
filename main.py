import numpy as np
from utils import *
from parameters import Parameters
from game import Game
import time

time_manual = []
time_minmax = []
time_alphabeta = []

boards_1 = []
boards_2 = []
def run_game(depth_1=None, depth_2=None):
    print("hello UI_assignment!")
    parameters = Parameters(depth=[depth_1, depth_2])
    print(parameters.depth)
    game = Game()
    board = np.zeros((parameters.height, parameters.width), dtype=int)

    full = np.zeros(parameters.width, dtype=int)
    while True:
        player = game.turn % 2 + 1
        move, move_time = get_move(parameters, full, board, player)
        if move == -1:
           continue

        row = board.shape[0] - full[move] - 1
        full[move] += 1
        # nariši premik na plošči
        board[row, move] = player
        print("----------------", full, parameters.height)
        #izriši ploščo
        show_board(board, game.turn, player)

        #preveri če je konec igre
        game.over = check_score(board, move, row, player, 4, parameters)
        game.turn += 1

        if parameters.players[player-1] == 'HUMAN':
            time_manual.append(move_time)
        elif parameters.players[player-1] == 'MINMAX':
            time_minmax.append(move_time)
        elif parameters.players[player-1] == 'ALPHABETA':
            time_alphabeta.append(move_time)
        if game.over:
            print("GAME OVER! Player", player, "(", parameters.players[player-1], ") won!")
            game.winner = player
            break
        if player == 1:
            boards_1.append(board)
        else:
            boards_2.append(board)
        time.sleep(.3)

    # Save times for of each algorithm
    if parameters.players[0] == 'ALPHABETA':
        np.save('time_res/time_alphabeta_'+str(parameters.depth[0]), np.array(time_alphabeta))
    elif parameters.players[0] == 'MINMAX':
        np.save('time_res/time_minmax_'+str(parameters.depth[0]), np.array(time_minmax))
    elif parameters.players[0] == 'HUMAN':
        np.save('time_res/time_manual', np.array(time_manual))

    if parameters.players[1] == 'ALPHABETA':
        np.save('time_res/time_alphabeta_'+str(parameters.depth[1]), np.array(time_alphabeta))
    elif parameters.players[1] == 'MINMAX':
        np.save('time_res/time_minmax_'+str(parameters.depth[1]), np.array(time_minmax))
    elif parameters.players[1] == 'HUMAN':
        np.save('time_res/time_manual', np.array(time_manual))

    # Save boards
    if game.winner == 1:
        save_boards(boards_1, 1, 1)
        save_boards(boards_2, 0, 2)
    else:
        save_boards(boards_1, 0, 1)
        save_boards(boards_2, 1, 2)


if __name__ == '__main__':
    run_game(4, 4)

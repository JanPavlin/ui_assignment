# MLP for Pima Indians Dataset Serialize to JSON and HDF5
from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_yaml
import numpy
import os
class Parameters():

    def __init__(self, depth=[4, 4]):
        self.width = 7
        self.height = 6
        self.depth = depth
        # Options are HUMAN, MINMAX, ALPHABETA, NEURAL
        self.players = ['HUMAN', 'MINMAX']

        try:
            self.model = self.load_model()
        except:
            pass

    def load_model(self):
        yaml_file = open(r'neural_network/model1.yaml', 'r')
        loaded_model_yaml = yaml_file.read()
        yaml_file.close()
        loaded_model = model_from_yaml(loaded_model_yaml)
        # load weights into new model
        loaded_model.load_weights("neural_network/model1.h5")
        print("Loaded model from disk")
        return loaded_model
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    mm = list()
    ab = list()
    depth = list(range(1, 7))

    for i in range(1, 7):
        minmax = np.load("time_res/time_minmax_"+str(i)+".npy")
        alphabeta = np.load("time_res/time_alphabeta_"+str(i)+".npy")
        print("::::::::::::::::::")
        print("Depth:", i)
        print("MM", minmax.mean())
        print("AB", alphabeta.mean())
        mm.append(minmax.mean())
        ab.append(alphabeta.mean())

    plt.plot(depth, ab, color='r', label="AlphaBeta time")
    plt.plot(depth, mm, color='b', label="MinMax time")
    plt.ylim(0, 24)
    plt.legend()
    plt.title("Time consumption of algorithms")
    plt.xlabel("Depth")
    plt.ylabel("Time required (in seconds) to select best move.")
    plt.savefig("Time consumption")
    plt.show()

    manual = np.load("time_res/time_manual.npy")


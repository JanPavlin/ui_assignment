import numpy as np
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten
from keras.utils import to_categorical
import random
import os
from os import listdir
from os.path import isfile, join
data_path = r'boards/'


def prepare_data():
    train_x = []
    train_y = []
    test_x = []
    test_y = []
    X = []
    Y = []
    split = 0.3
    win_path = data_path+"win/"
    lose_path = data_path+"lose/"
    winfiles = [f for f in listdir(win_path) if isfile(join(win_path, f))]
    losefiles = [f for f in listdir(lose_path) if isfile(join(lose_path, f))]
    for filename in losefiles:
        player = int(filename.split(".")[0][-1])
        board = np.load(lose_path+filename)
        if player == 1:
            board[board==2] = -1
        elif player == 1:
            board[board == 1] = -1
        X.append(board)
        Y.append(0)
        if split < random.random():
            train_x.append(board)
            train_y.append(0)
        else:
            test_x.append(board)
            test_y.append(0)


    for filename in winfiles:
        player = int(filename.split(".")[0][-1])
        board = np.load(win_path+filename)
        if player == 1:
            board[board==2] = -1
        elif player == 1:
            board[board == 1] = -1

        if split > random.random():
            train_x.append(board)
            train_y.append(1)
        else:
            test_x.append(board)
            test_y.append(1)
        X.append(board)
        Y.append(1)
    X = np.array(X)
    Y = np.array(Y)
    from sklearn.utils import shuffle
    X, Y = shuffle(X, Y)
    train_x = X[:2*X.shape[0]//3]
    train_y = Y[:2*Y.shape[0]//3]

    test_x = X[2 * X.shape[0] // 3:]
    test_y = Y[2 * Y.shape[0] // 3:]


    return np.array(train_x), np.array(train_y), np.array(test_x), np.array(test_y)


def nn_learn():
    train_x, train_y, test_x, test_y = prepare_data()

    train_x = np.expand_dims(train_x, axis=3)
    test_x = np.expand_dims(test_x, axis=3)

    train_y = np.expand_dims(train_y, axis=1)
    test_y = np.expand_dims(test_y, axis=1)
    # print(test_y)
    # print(train_y)
    # exit()
    num_filters = 4
    filter_size = 4
    pool_size = 2

    # Build the model.
    model = Sequential([
        Conv2D(num_filters, filter_size, input_shape=(6, 7, 1)),
        MaxPooling2D(pool_size=pool_size),
        Flatten()
    ])

    model.add(Dense(1, kernel_initializer='normal'))
    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam')
    from keras.callbacks import EarlyStopping
    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10)
    # Train the model.
    model.fit(
        train_x,
        train_y,
        epochs=1000,
        batch_size=8,
        callbacks=[es],
        validation_data=(test_x, test_y),

    )

    # Save the model to disk.
    #model.save_weights(r'/neural_network/cnn.h5')
    with open(r'neural_network/model1.yaml', "w") as yaml_file:
        yaml_file.write(model.to_yaml())
    # serialize weights to HDF5
    model.save_weights(r'neural_network/model1.h5')
    print("Saved model to disk")

    predictions = model.predict(test_x)
    print(predictions)


if __name__ == '__main__':
    nn_learn()
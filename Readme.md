# `CONNECT4 - Python`
#### `Required libraries: `
To run the program you will need the latest versions of the following libraries: 

* numpy 

* keras 

* matplotlib

#### `Setting up the game:`
To set up the game, you will first need to open file **_parameters.py_** and in line 14 set the players. 
You can chose between '_HUMAN_', '_MINMAX_', '_ALPHABETA_' and '_NEURAL_'. Set one of them as player one and one as player two. 
Default settings are that player 1 is human and player 2 is MinMax algorithm. 

To set up the depth of AlphaBeta and MinMax algorithm you need to open file **_main.py_** and in the last, 80th row 
you can change depths. In function _run_game(d1, d2)_, first parameter is the selected depth of first player and second parameter as depth of the second player. 
Default depths are set at 4. Those parameters impact only performance and time consumption of MinMax and AlphaBeta algorithm 
and do not affect other types of players. 

#### `Running the game:` 
To run the game, you must run file **_main.py_**. The game will show in terminal. 


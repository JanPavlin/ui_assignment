import math
import numpy as np
import copy

def heuristic(parameters, board):
    #print(board, board.shape)
    (p_h, p_w) = board.shape
    heur = 0
    state = board
    printable = False
    return_test = True
    if printable: print("SHAPE:" ,p_w, p_h)
    for i in range(0, p_h):
        for j in range(0, p_w):
            # check horizontal streaks
            try:
                pass
                if printable: print(i,j)
                #if printable: print(state[i][j] == state[i + 1][j] == 2)
            except:
                pass
            try:
                # add player one streak scores to heur
                if i + 1 < p_h:

                    if state[i][j] == state[i + 1][j] == 1:
                        heur += 10
                        if printable: print(1)
                    if state[i][j] == state[i + 1][j] == 2:
                        heur -= 10
                        if printable: print(4)
                if i + 2 < p_h:
                    if state[i][j] == state[i + 1][j] == state[i + 2][j] == 1:
                        heur += 100
                        if printable: print(2)
                    if state[i][j] == state[i + 1][j] == state[i + 2][j] == 2:
                        heur -= 100
                        if printable: print(5)
                if i + 3 < p_h:
                    if state[i][j] == state[i + 1][j] == state[i + 2][j] == state[i + 3][j] == 1:
                        heur += 500
                        if printable: print(3)
                        if return_test: return 100000
                    if state[i][j] == state[i + 1][j] == state[i + 2][j] == state[i + 3][j] == 2:
                        heur -= 500
                        if printable: print(6)
                        if return_test: return -100000
            except IndexError as e:
                if printable: print(1, e, i, j, "||", p_h, p_w)

            # check vertical streaks
            try:
                # add player one vertical streaks to heur
                if j + 1 < p_w and state[i][j] == state[i][j + 1] == 1:
                    heur += 10
                    if printable: print(7)
                if j + 1 < p_w and state[i][j] == state[i][j + 1] == 2:
                    heur -= 10
                    if printable: print(10)

                if j + 2 < p_w and state[i][j] == state[i][j + 1] == state[i][j + 2] == 1:
                    heur += 100
                    if printable: print(8)
                if j + 2 < p_w and state[i][j] == state[i][j + 1] == state[i][j + 2] == 2:
                    heur -= 100
                    if printable: print(11)
                    if return_test: return 100000
                if j + 3 < p_w and state[i][j] == state[i][j + 1] == state[i][j + 2] == state[i][j + 3] == 1:
                    heur += 500
                    if printable: print(9)
                if j + 3 < p_w and state[i][j] == state[i][j + 1] == state[i][j + 2] == state[i][j + 3] == 2:
                    heur -= 500
                    if printable: print(12)
                    if return_test: return -100000
            except IndexError as e:
                if printable: print(2, e, i, j)

            # check positive diagonal streaks
            try:
                # add player one streaks to heur
                if j + 1 < p_w and i + 1 < p_h and state[i][j] == state[i + 1][j + 1] == 1:
                    heur += 10
                    if printable: print(13)
                if j + 1 < p_w and i + 1 < p_h and state[i][j] == state[i + 1][j + 1] == 2:
                    heur -= 10
                    if printable: print(16)

                if j + 2 < p_w and i + 2 < p_h and state[i][j] == state[i + 1][j + 1] == state[i + 2][j + 2] == 1:
                    heur += 100
                    if printable: print(14)
                if j + 2 < p_w and i + 2 < p_h and state[i][j] == state[i + 1][j + 1] == state[i + 2][j + 2] == 2:
                    heur -= 100
                    if printable: print(17)

                if j + 3 < p_w and i + 3 < p_h and state[i][j] == state[i + 1][j + 1] == state[i + 2][j + 2] \
                        == state[i + 3][j + 3] == 1:
                    heur += 500
                    if printable: print(15)
                    if return_test: return 100000
                if j + 3 < p_w and i + 3 < p_h and state[i][j] == state[i + 1][j + 1] == state[i + 2][j + 2] \
                        == state[i + 3][j + 3] == 2:
                    heur -= 500
                    if printable: print(18)
                    if return_test: return -100000
            except IndexError as e:
                if printable: print(3, e, i, j)


            try:
                # add  player one streaks
                if j - 1 >= 0 and i + 1 < p_h and state[i][j] == state[i + 1][j - 1] == 1:
                    heur += 10
                    if printable: print(19)
                if j - 1 >= 0 and i + 1 < p_h and state[i][j] == state[i + 1][j - 1] == 2:
                    heur -= 10
                    if printable: print(22)

                if j - 2 >= 0 and i + 2 < p_h and state[i][j] == state[i + 1][j - 1] == state[i + 2][j - 2] == 1:
                    heur += 100
                    if printable: print(20)
                if j - 2 >= 0 and i + 2 < p_h and state[i][j] == state[i + 1][j - 1] == state[i + 2][j - 2] == 2:
                    heur -= 100
                    if printable: print(23)

                if j - 3 >= 0 and i + 3 < p_h and state[i][j] == state[i + 1][j - 1] == state[i + 2][j - 2] \
                        == state[i + 3][j - 3] == 1:
                    heur += 500
                    if printable: print(21)
                    if return_test: return 100000
                if j - 3 >= 0 and i + 3 < p_h and state[i][j] == state[i + 1][j - 1] == state[i + 2][j - 2] \
                        == state[i + 3][j - 3] == 2:
                    heur -= 500
                    if printable: print(24)
                    if return_test: return -100000
            except IndexError as e:
                if printable: print(4,e, i, j)
    #print("__----_")
    #print(board)
    #print("RES:", heur)
    return heur


def miniMax(parameters, board, full, depth, player):
    #print("_________________________________________", full, depth, player)
    if depth == 0:
        neki = heuristic(parameters, board), -1
        #Rezultat mreže

        return neki
    #print("PLAYER: ", player)
    if player == 1:

        bestScore = -math.inf
        shouldReplace = lambda x: x > bestScore
    else:
        bestScore = math.inf
        shouldReplace = lambda x: x < bestScore
    bestMove = -1
    children = possibilities(board, full, player)
    for child in children:
        move, childboard, child_full = child
        temp = miniMax(parameters, childboard, child_full, depth - 1, player % 2 + 1)[0]

        #print(depth, temp)
        if shouldReplace(temp):
            bestScore = temp
            bestMove = move
        #print(depth, bestScore)
    #print(bestScore)
    return bestScore, bestMove

def makeMove(child, full, i, player):
    move = i
    row = child.shape[0] - full[move] - 1
    # nariši premik na plošči
    child[row, move] = player
    #print("i:", i, "row:", row, "full", full, player)
    full[move] += 1

    return child, full


def possibilities(board, full, player):
    children = []
    for i in range(board.shape[1]):
        if full[i] <= board.shape[0] - 1:
            child = copy.copy(board)
            child_full = copy.copy(full)
            child, child_full = makeMove(child, child_full, i, player)
            children.append((i, child, child_full))
    return children


def alphaBeta(parameters, board, full, depth, player, alpha, beta):
    #print("AB:", depth, player, alpha, beta)
    if depth == 0:
        return heuristic(parameters, board)*depth, -1

    if player == 1:
        bestScore = -math.inf
        shouldReplace = lambda x: x > bestScore
    else:
        bestScore = math.inf
        shouldReplace = lambda x: x < bestScore
    bestMove = -1

    children = possibilities(board, full, player)
    for child in children:
        move, childboard, child_full = child
        #print(parameters, childboard, child_full, depth, (player) %2 + 1, alpha, beta)
        temp = alphaBeta(parameters, childboard, child_full, depth - 1, (player) % 2 + 1, alpha, beta)[0]
        if shouldReplace(temp):
            bestScore = temp
            bestMove = move
        if player:
            alpha = max(alpha, temp)
        else:
            beta = min(beta, temp)
        if alpha >= beta:
            break
    return bestScore, bestMove


def neural_score(model, board):
    board = np.expand_dims(board, axis=0)
    board = np.expand_dims(board, axis=3)
    print("prediction:", model.predict(board))
    return model.predict(board)

def neural(parameters, board, full, player):
    children = possibilities(board, full, player)
    best_score = 0
    best_move = 0
    print("SCORES PER COLUMN:")
    for child in children:
        move, childboard, child_full = child
        # print(parameters, childboard, child_full, depth, (player) %2 + 1, alpha, beta)
        if player == 1:
            childboard[childboard == 2] = -1
        elif player == 2:
            childboard[childboard == 1] = -1
            childboard[childboard == 2] = 1
        temp = float(neural_score(parameters.model, childboard)[0][0])
        print(temp)

        if temp > best_score:
            best_score = temp
            best_move = move


    return best_move
if __name__ == '__main__':
    neki = [[0,1,2,1],
            [0,2,1,1],
            [2,1,0,1]]
    heuristic(None, np.array(neki))



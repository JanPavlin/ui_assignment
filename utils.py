import numpy as np
import keyboard
from tmp import *
import time


def show_board(board, turn, player):
    print("____________________________")
    print("------TURN: "+str(turn)+"-------" ,"\n Player:", player)
    print(board)


def check_score(grid, col, row, turn, winAmount, parameters):
    return (checkWinInRow(grid[row, :], turn, winAmount, "check row")
            or checkWinInRow(grid[:, col], turn, winAmount, "check column")
            or checkWinInRow(np.diagonal(grid, offset=col - row), turn, winAmount, "check diagonal 1")
            or checkWinInRow(np.diagonal(np.fliplr(grid), offset=parameters.width - 1 - (col + row)),
                             turn, winAmount, "check diagonal2"))


def checkWinInRow(line, turn, winAmount, desc):
    count = 0
    for c in range(len(line)):
        if line[c] == turn:
            count += 1
            if count >= winAmount:
                return True
        else:
            count = 0
    return False


def get_move(parameters, full, board, player):
    if parameters.players[player-1] == 'HUMAN':
        st = time.time()
        move = input("Place new piece: " )
        et = time.time()
        # Naredi premik
        if move not in [str(i) for i in range(parameters.width+1)]:
            print("WRONG KEY, you pressed:", move)
            return -1, 0

        if full[int(move)-1] >= parameters.height:
            print("This column has already reached the top!")
            return -1, 0

        else:
            return int(move) -1, et-st

    elif parameters.players[player-1] == "MINMAX":
        st = time.time()
        score = miniMax(parameters, board, full, parameters.depth[player-1], player)
        et = time.time()
        move = score[1]
        print("MOVE:", move)


        if full[int(move)] >= parameters.height:
            print("This column has already reached the top!")
            print("Breaking because infinite loop.")
            return exit()
        return move, et-st

    elif parameters.players[player-1] == "ALPHABETA":
        st = time.time()
        score = alphaBeta(parameters, board, full, parameters.depth[player-1], player, -math.inf, math.inf)
        et = time.time()
        print(score)
        move = score[1]

        if full[int(move)] >= parameters.height:
            print("This column has already reached the top!")
            print("Breaking because infinite loop.")
            return exit()
        return move, et-st

    elif parameters.players[player-1] == "NEURAL":
        st = time.time()
        score = neural(parameters, board, full, player)
        et = time.time()
        print(score)
        move = score


        if full[int(move)] >= parameters.height:
            print("This column has already reached the top!")
            print("Breaking because infinite loop.")
            return exit()
        return move, et-st

    else:
        return -1, 0

def save_boards(boards, WL, player):
    import string
    import random
    r = 10
    print("Saving_boards")
    if WL == 1:
        path = r'boards/win/'
    else:
        path = r"boards/lose/"

    for board in boards:
        b_name = path+''.join(random.choice(string.ascii_uppercase) for _ in range(r)) + "_" + str(player)
        np.save(b_name, board)